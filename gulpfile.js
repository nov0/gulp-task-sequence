var gulp = require('gulp');
var child = require('child_process');

gulp.task('first-task', function(done) {
	child.spawn('echo', ['first task'], {stdio: 'inherit'})
	.on('close', done);
});

gulp.task('second-task', ['first-task'], function(done) {
	child.spawn('echo', ['second gulp task'], {stdio: 'inherit'})
	.on('close', done);
});

gulp.task('sequence', ['first-task', 'second-task']);
