To enable gulp in our project we need to add dependency with npm:

```npm install gulp -save``` - - this will add dependency to package.json file.

Now, we need to create <strong>gulpfile.js</strong> for gulp tasks.

Gulp configuration is simple, we need inject gulp to configuration file:

```javascript
var gulp = require('gulp');
```
and

```javascript
var child = require('child_process');
```

Now create first task:

```javascript
gulp.task('first-task', function(done) {
	child.spawn('echo', ['first task'], {stdio: 'inherit'})
	.on('close', done);
});
```

In this code we have ```'first-task'``` - that is name of task, in function we provide <strong>done</strong> witch is a <em>callback</em> function. In <strong>child.spawn</strong> we execute a native bash commands, in square braces is arguments for that command. With <strong>.on('close', done)</strong> we ensure that done (callback function) is executed after echo command.

And we crate second task:

```javascript
gulp.task('second-task', ['first-task'], function(done) {
	child.spawn('echo', ['second gulp task'], {stdio: 'inherit'})
	.on('close', done);
});
```

Only difference in this tasks are that second-task have tasks (in this case first-task) that need to be executed before second task. With this we assure that second task is not executed before first task.

And at the end we can create one task that start both tasks sequentially:

```javascript
gulp.task('sequence', ['first-task', 'second-task']);
```

you can run it with command:
```gulp sequence```

Output will look something like this:
```bash
[15:06:25] Using gulpfile ~/gulp-tasks/gulpfile.js
[15:06:25] Starting 'first-task'...
first task
[15:06:25] Finished 'first-task' after 4.29 ms
[15:06:25] Starting 'second-task'...
second gulp task
[15:06:25] Finished 'second-task' after 2.04 ms
[15:06:25] Starting 'sequence'...
[15:06:25] Finished 'sequence' after 20 μs
```